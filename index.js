function createNewUser () {
    let firstName = prompt("Write your first name:") ;
    let surName = prompt("Write your surname:") ;
    let birthday = prompt("Write your date of birth (dd.mm.yyyy):");

let user = {
    firstName: firstName,
    surName: surName,
    birthday: birthday,
    getLogin: function () {
        return (this.firstName.charAt(0) + this.surName).toLowerCase();
    },
    setFirstName: function (newFirstName) {
        Object.defineProperty(this, "firstName", {
            writable: false
        });
        this.firstName = newFirstName;
    },
    setSurName: function (newSurName) {
        Object.defineProperty(this, "surName", {
            writable: false
        })
        this.surName = newSurName;
    },
        getAge: function () {
            let today = new Date();
            let parts = this.birthday.split(".");
            let birthDate = new Date(parts[2], parts[1] - 1, parts[0]);
            let age = today.getFullYear() - birthDate.getFullYear();
            let month = today.getMonth() - birthDate.getMonth();
            if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            return age;
        },

    getPassword: function () {
        let firstLetter = this.firstName.charAt(0).toUpperCase();
        let surNameLower = this.surName.toLowerCase();
        let year = this.birthday.slice(-4);
        return firstLetter + surNameLower + year;
    }
} ;
 return user;
}
let newUser = createNewUser();

console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());

